#!/bin/bash

echo "********************"
echo "** Pushing image ***"
echo "********************"

IMAGE="maven-project"

echo "** Logging in ***"
docker login -u rohin31 -p $PASS
echo "*** Tagging image ***"
docker tag $IMAGE:$BUILD_TAG rohin31/$IMAGE:$BUILD_TAG
echo "*** Pushing image ***"
docker push rohin31/$IMAGE:$BUILD_TAG
